#!/usr/bin/env python3
# File: populationsplitter.py
# Author: s2180774
# Date: 27-03-2017
# Splits a population in strata
# USAGE: python3 populationsplitter.py OPTION (male or female)

import sys



def main(argv):
    for line in sys.stdin:
        name = sys.argv[1]
        try:
            splitline = line.strip().split('\t')
            gender = splitline[0]
            if name == gender:
                print(line.strip())
        except:
            continue

if __name__ == '__main__':
    main(sys.argv)
