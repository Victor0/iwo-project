# iwo-project
Final code online
To run start ./ResearchTweets.sh
Repository for course LIX024P05

The primary objective of this course is to acquaint students with fundamentals of carrying out research.
Specifically, the course introduces them to basic terminology in research, the scientific research process itself, and research methods applied in research in information science.