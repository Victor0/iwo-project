#!/usr/bin/env python3
# File: frequencies_initial.py
# Author: s2180774
# Date: 27-03-2017
# Counts the presence of mentions (0, 1) for two groups (MALES & FEMALES)
# in a population. Prints the result in a neat table using Tabulate
# USAGE: python3 frequencies_initial.py

import sys
import io
import random

def randomSampler(lines):
    """
    Function for random sampling a line out of the list lines
    """
    length = len(lines)                 # Number of posibilities
    ind = random.randrange(length)      # Random draw from numbers
    return lines[ind]                   # Returns random line
            
def updateDic(measure,dic):
    """
    Function takes a line (measure) and dictionary to store the mention counts
    """
    gender = measure[0]                 # Either 'male' or 'female'
    mentions = int(measure[1])          # This counts how many mentions the line has
    if gender == "male":                # If measure is from a male
        dic["male"] += 1
        if mentions > 0:               # If count of mentions is 1
            dic["male_mention"] += 1
    elif gender == "female":                               # If measure is not male, it is female
        dic["female"] += 1
        if mentions > 0:
            dic["female_mention"] += 1
    else:
        pass
    # The lines below calculate messages without mentions
    # It substracts count for messages with mention from all measures.
    dic["male_without"] = dic["male"] - dic["male_mention"]
    dic["female_without"] = dic["female"] - dic["female_mention"]
    # Update the measures frequency in the dictionary
    return dic

def groupReader(path):
    """
    Function to easily read a file and format it for frequency analysis
    """
    with open(path,'r',encoding="utf-8") as f:
        lines = f.read()
    lines = lines.strip().split('\n')
    lines = [line.strip().split('\t') for line in lines]
    return lines

def main():
    # Results dictionary for counting the frequencies later
    results = {'male':0,"female":0,"male_mention":0,"female_mention":0,"male_without":0,"female_without":0}

    # Reading, stripping and splitting in 1 line per file with groupReader()
    males = groupReader('MALES') 
    females = groupReader('FEMALES')

    # Simple random sampling starts
    samples = int(200)                # Number of samples; here 200
    samplesize = int(100)               # Sample size; here 100
    
    for i in range(samples):
        for i in range(samplesize):
            measure = randomSampler(males)      # Take random line from males
            updateDic(measure,results)          # Update result with random line's count
            # Do the same for the female group
            measure = randomSampler(females)
            updateDic(measure,results)

    # These 3 lines are used for pretty table printing the results with the Tabulate module.
    rownames = ("No mentions","Mentions")
    malesitem = (results['male_without'],results['male_mention'])
    femalesitem = (results['female_without'],results['female_mention'])
    # These dictionaries are for storing the results proportions for more insight.
    maleprop = []
    femaleprop = []
    for i in range(2):
        mprop = malesitem[i]/results['male']
        fprop = femalesitem[i]/results['female']
        mprop = '{:.2f}'.format(mprop)
        fprop = '{:.2f}'.format(fprop)
        maleprop.append(mprop)
        femaleprop.append(fprop)

    for i,j in (("Counts",rownames),("Male",malesitem),("Female",femalesitem)):
        print("{:<10}{:<15}{:<15}".format(i,j[0],j[1]))
    print()
    for i,j in (("Counts",rownames),("Male",maleprop),("Female",femaleprop)):
        print("{:<10}{:<15}{:<15}".format(i,j[0],j[1]))

main()

