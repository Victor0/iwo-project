import sys
import io
import os

female=[name.strip() for name in open("names/females.txt","r",encoding="UTF-8").readlines()]
male=[name.strip() for name in open("names/males.txt","r",encoding="UTF-8").readlines()]
unwanted=[name.strip() for name in open("names/unwanted.txt","r",encoding="UTF-8").readlines()]

femaleset = set(female)
maleset = set(male)
unwantedset = set(unwanted)

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
for line in input_stream:
    name = line.strip().split("\t")[0].lower() # assumes name is first in tab-separated input
    restline = line.strip().split("\t")[1:]
    rst = "".join(c for c in restline)
    rt = rst.split()
    #rest = len(rt)
    f=0
    m=0
    if name.split():
        all_names = name.lower().split()
        for n in femaleset:
            for i in range(len(all_names)):
                if n == all_names[i]:
                    f+=1
        for n in maleset:
            for i in range(len(all_names)):
                if n == all_names[i]:
                    m+=1
        for n in unwantedset:
            for i in range(len(all_names)):
                if n == all_names[i]:
                    m=f=0
    if f > m:
        print("female\t{}".format(len(rt)))
    elif m > f:
        print("male\t{}".format(len(rt)))
    else:
        pass
