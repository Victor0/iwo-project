#!/usr/bin/env python3
# File: frequencies_explore.py
# Author: s2180774
# Date: 27-03-2017
# Counts the frequencies of mentions (0, 1 or >1) for two groups (MALES & FEMALES)
# in a population. Prints the result in a neat table using Tabulate
# USAGE: python3 frequencies_explore.py

import sys
import io
import random
#from tabulate import tabulate

def randomSampler(lines):
    """
    Function for random sampling a line out of the list lines
    """
    length = len(lines)                 # Number of posibilities
    ind = random.randrange(length)      # Random draw from numbers
    return lines[ind]                   # Returns random line
            
def updateDic(measure,dic):
    """
    Function takes a line (measure) and dictionary to store the mention counts
    """
    gender = measure[0]                 # Either 'male' or 'female'
    mentions = int(measure[1])          # This counts how many mentions the line has
    if gender == 'male':                # If measure is from a male
        dic['male'] += 1
        if mentions == 1:               # If count of mentions is 1
            dic['male_1mention'] += 1
        elif mentions > 1:              # If count of mentions is more than one
            dic['male_more'] += 1
    elif gender == "female":                               # If measure is not male, it is female
        dic['female'] += 1
        if mentions == 1:
            dic['female_1mention'] += 1
        elif mentions > 1:
            dic['female_more'] += 1

    # The lines below calculate messages without mentions
    # It substracts count for messages with mention from all measures.
    dic['male_without'] = dic['male'] - (dic['male_1mention'] + dic['male_more'])
    dic['female_without'] = dic['female'] - (dic['female_1mention'] + dic['female_more'])
    # Update the measures frequency in the dictionary
    return dic

def groupReader(path): # Creates a strata from the population
    """
    Function to easily read a file and format it for frequency analysis
    """
    with open(path,'r',encoding="utf-8") as f:
        lines = f.read()
    lines = lines.strip().split('\n')
    lines = [line.strip().split('\t') for line in lines]
    return lines

def main():
    # Results dictionary for counting the frequencies later
    results = {'male':0,"female":0,"male_1mention":0,"female_1mention":0,"male_without":0,"female_without":0,"male_more":0,"female_more":0}

    # Reading, stripping and splitting in 1 line per file with groupReader()
    males = groupReader('MALES')        # Strata 1
    females = groupReader('FEMALES')    # Strata 2

    # Simple random sampling starts
    samples = int(200)                 # Number of samples; here 200
    samplesize = int(100)               # Sample size; here 100

    for i in range(samples):
        for i in range(samplesize):             # Stratum
            measure = randomSampler(males)      # Take random line from males
            updateDic(measure,results)          # Update result with random line's count
            # Do the same for the female group
            measure = randomSampler(females)
            updateDic(measure,results)

    # These 3 lines are used for pretty table printing the results with the Tabulate module.
    rownames = ("Mentions = 0","Mentions = 1","Mentions > 1")
    malesitem = (results['male_without'],results['male_1mention'],results['male_more'])
    femalesitem = (results['female_without'],results['female_1mention'],results['female_more'])

    # These dictionaries are for storing the results proportions for more insight.
    maleprop = []
    femaleprop = []

    for i in range(3):
        mprop = malesitem[i]/results['male']
        fprop = femalesitem[i]/results['female']
        mprop = '{:.2f}'.format(mprop)
        fprop = '{:.2f}'.format(fprop)
        maleprop.append(mprop)
        femaleprop.append(fprop)

    for i,j in (("Counts",rownames),("Male",malesitem),("Female",femalesitem)):
        print("{:<10}{:<15}{:<15}{:<15}".format(i,j[0],j[1],j[2]))
    print()
    for i,j in (("Counts",rownames),("Male",maleprop),("Female",femaleprop)):
        print("{:<10}{:<15}{:<15}{:<15}".format(i,j[0],j[1],j[2]))


main()

