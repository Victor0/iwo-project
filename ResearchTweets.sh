#/bin/bash
# Victor0
# Analyses twitter data using Python scripts.
# Temporary variables for the different samples
SAMPLE1=`mktemp`
SAMPLE2=`mktemp`
SAMPLE3=`mktemp`
FINALPOPULATION=`mktemp`

echo "Starting data collection of 3 samples [20-30 seconds]"
echo
# Take data | select user.name and mentions | filter data that for which the gender can be determined > save sample as SAMPE1
zless /net/corpora/twitter2/Tweets/2017/01/20170109\:11.out.gz | /net/corpora/twitter2/tools/tweet2tab user.name mentions > $SAMPLE1
echo "First sample was succesfully saved. Messages:"
cat $SAMPLE1 | wc -l
echo

# Take new data | select user.name and mentions | filter data that for which the gender can be determined > save sample as SAMPE2
zless /net/corpora/twitter2/Tweets/2017/01/20170119\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab user.name mentions > $SAMPLE2
echo "Second sample was succesfully saved. Messages:"
cat $SAMPLE2 | wc -l
echo

# Take new data | select user.name and mentions | filter data that for which the gender can be determined > save sample as SAMPE3
zless /net/corpora/twitter2/Tweets/2017/02/20170218\:13.out.gz | /net/corpora/twitter2/tools/tweet2tab user.name mentions > $SAMPLE3
echo "Third sample was succesfully saved. Messages:"
cat $SAMPLE3 | wc -l
echo

# All the SAMPLES are pasted together into a large heterogeneous dataset (it contains measurements for both the male and female group)
paste -s -d'\n' $SAMPLE1 $SAMPLE2 $SAMPLE3 > $FINALPOPULATION
rm $SAMPLE1 $SAMPLE2 $SAMPLE3
echo "Combined all 3 samples. Messages to process:"
cat $FINALPOPULATION | wc -l
echo
echo "Processing messages, automatic gender annotation starting. This takes a while [2-3 minutes]"
cat $FINALPOPULATION | python3 defgender.py > FINALSAMPLE
rm $FINALPOPULATION
echo "Success!"
echo "Thanks for your patience. The population is almost ready for sampling frequencies."
echo "Now splitting the population into gender group strata"
echo
# Splitting the population into homogeneous groups, MALES and FEMALES for stratification, the outputs are called strata
cat FINALSAMPLE | python3 populationsplitter.py male > MALES
echo "Males strata is ready, males in group:"
cat MALES | wc -l
cat FINALSAMPLE | python3 populationsplitter.py female > FEMALES
echo "Female strata is ready, females in group:"
cat FEMALES | wc -l

# Removing the combined sample since it is not needed anymore
rm FINALSAMPLE

echo "Starting analysis with frequency_initial.py"
echo "Frequencies gender and mention or no mention (N=200, n=100):"
echo
# Program uses stratified random sampling, sample size of 500, 1000 samples
python3 frequency_initial.py
echo
echo "Exploring mention frequencies with frequency_explore.py"
# Program also uses stratified random sampling, sample size of 500, 1000 samples
echo "Frequencies gender, 1 mention or more than 1 mentions (N=200, n=100)):"
python3 frequency_explore.py
echo

while true; do
    read -p "Do you wish to draw another stratified sample? " yn
    case $yn in
        [Yy]* ) echo "Frequencies gender and mention or no mention (N=200, n=100)):"
        	python3 frequency_initial.py
  		echo
 		echo "Frequencies gender, 1 mention or more than 1 mentions (N=200, n=100)):"
  		python3 frequency_explore.py
                echo;;
        [Nn]* ) rm MALES FEMALES
		exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
