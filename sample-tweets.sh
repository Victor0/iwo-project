#/bin/bash
# Victor0
# Temporary variables for the different queries
ALL_TWEETS=`mktemp`
UNIQ_TWEETS=`mktemp`
ALL_RT=`mktemp`
UNIQ_NORT=`mktemp`

echo "How many tweets are in the sample?"
# Storing all the tweets with tweet2tab in ALL_TWEETS
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text > $ALL_TWEETS
# Printing the tweets/lines count
cat $ALL_TWEETS | wc -l

echo "How many unique tweets are in the sample?"
# Take all tweets | replace the duplicates (awk was tip from student) and store in UNIQ_TWEETS
cat $ALL_TWEETS | awk '!x [$0]++' > $UNIQ_TWEETS
# Printing the unique tweets/lines count
cat $UNIQ_TWEETS | wc -l

echo "How many retweets are in the sample (out of the unique tweets)?"
# Take all unique tweets and store lines starting with RT in ALL_RT
cat $UNIQ_TWEETS | grep '^RT' > $ALL_RT
# Printing line count retweets
cat $ALL_RT | wc -l

echo "Show the first 20 unique tweets in the sample that are not retweets."
# Take all lines out of UNIQ_TWEETS that are in ALL_RT, store RT-less in UNIQ_NORT
grep -F -x -v -f $ALL_RT $UNIQ_TWEETS > $UNIQ_NORT
# Print the first 20 with head
cat $UNIQ_NORT | head -n 20

# remove the temporary variables
rm -f $ALL_TWEETS
rm -f $UNIQ_TWEETS
rm -f $ALL_RT
rm -f $UNIQ_NORT